use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::seq::SliceRandom;
use rand::thread_rng;
use tera::{Context, Tera};

// Function to return a random song
async fn song_of_the_day() -> impl Responder {
    let songs = ["I Really Like You", "Flowers", "Sunshine", "Summer"];
    // Randomly select a song from the array
    let song = songs
        .choose(&mut thread_rng())
        .unwrap_or(&"No song found")
        .to_string();

    // Render the template with the song title
    let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/static/root/*")).unwrap();
    let mut context = Context::new();
    context.insert("song", &song);
    let rendered = tera.render("song_of_the_day.html", &context).unwrap();

    HttpResponse::Ok().body(rendered)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/song_of_the_day", web::get().to(song_of_the_day))
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind("0.0.0.0:50505")?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{http, test, web, App};

    #[actix_web::test]
    async fn test_song_of_the_day() {
        let mut app = test::init_service(
            App::new().route("/song_of_the_day", web::get().to(song_of_the_day)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/song_of_the_day")
            .to_request();
        let resp = test::call_service(&mut app, req).await;
        assert!(resp.status().is_success());
    }
}
