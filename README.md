## Purpose
The goal of this project is to containerize a Rust Actix Web Service. I build a simple web app that will randomly generate a song for that day.


## Preparation
### Web App
**The web page for generating the song of the day**
![Screenshot_2024-02-23_at_2.46.50_PM](/uploads/fa25c478e579fe3bf0e6c742182fc184/Screenshot_2024-02-23_at_2.46.50_PM.png)
![Screenshot_2024-02-23_at_2.46.41_PM](/uploads/8c9f2926d7c4120eea1e109141e83d8c/Screenshot_2024-02-23_at_2.46.41_PM.png)

**Docker Image**
![Screenshot_2024-02-23_at_2.46.24_PM](/uploads/0a9415d7e88fe2c12454db32fd4fc515/Screenshot_2024-02-23_at_2.46.24_PM.png)

**Docker Container**
![Screenshot_2024-02-23_at_2.46.24_PM](/uploads/75d2dbe957f92d440c1abc2e10389e14/Screenshot_2024-02-23_at_2.46.24_PM.png)

## Steps
1. run cargo new <insert project name here>
2. add depedenecies found in my cargo.toml file
3. build your web app
4. run your web app first before contanierizing via cargo run and do make all to check everything works fine (assuming you copy my Makefile)
5. after confirming it runs, make a Dockerfile
6. build docker image: docker build -t <insert any name here> .
7. deploy docker container to make sure it works: docker run -d -p 50505 <insert image name here>
8. push and run cicd